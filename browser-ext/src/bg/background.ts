//import { normalize } from "path";

// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });


//example of using a message handler from the inject scripts
//chrome.extension.onMessage.addListener(
//  function(request, sender, sendResponse) {
//  	chrome.pageAction.show(sender.tab.id);
//    sendResponse();
//  });


/*
Intercepting websocket requests involved injecing a little bit of js. Twitch doesn't like this, so some hoop jumping is required.
*/

// https://eternalreplay.com/twitch/campaigns

/*
Some helper stuff
 */

import moment from 'moment'; 

let CONFIG = {
  chests: false,
  enabled: true,
  silver: "smart"
}

let WSLOG = {}

function updateTwitchSeen(url: string) {
  try {
    let elements = url.split("/")
    const user = elements[elements.length - 1]
    WSLOG[user] = moment.utc().valueOf()
  } catch(e) {

  }
}

function checkHealth(tab: chrome.tabs.Tab) {
  console.log(`Running healthcheck: ${JSON.stringify(WSLOG)}`)
  try {
    let elements = tab.url.split("/")
    const user = elements[elements.length - 1]
    if(!WSLOG[user] || WSLOG[user] < moment.utc().valueOf() - 1000 * 60) {
      console.log(`Healthcheck - ${user} has not recieved messages for over a minute, refreshing tab`)
      chrome.tabs.reload(tab.id)
    }
  } catch (e) 
  {
    
  }
}

chrome.storage.onChanged.addListener(function (changes, area) {
  if (area == "local" && changes["extension-config"]) {
    const newValue = changes["extension-config"].newValue
    console.log(`Onchange fired, beautiful changes ${JSON.stringify(newValue)}`);
    CONFIG.chests = newValue.chests == "yes"
    CONFIG.enabled = newValue.enabled == "yes"
    CONFIG.silver = newValue.silver
  }
})

async function loadConfig() {
  await new Promise(function (resolve, reject) {
    chrome.storage.local.get(["extension-config"], function (result) {
      let defaultConfig = {
        chests: "no",
        enabled: "yes",
        silver: "smart"
      }
      if (result["extension-config"]) {
        defaultConfig = result["extension-config"]
      }

      CONFIG.chests = defaultConfig.chests == "yes"
      CONFIG.enabled = defaultConfig.enabled == "yes"
      CONFIG.silver = defaultConfig.silver

      resolve(CONFIG)

    })
  })
}

function level(d: IJsonCampaign): string {
  const types = ["bronze", "silver", "gold", "diamond"]
  const level = types.findIndex((t) => { return d.campaignType.toLowerCase().includes(t) })
  return types[level]
}

function levelRank(d: string): number {
  const types = ["bronze", "silver", "gold", "diamond"]
  return types.findIndex((t) => { return d == t })
}

function shouldSwitch(
  watchingNow: IInternalCampaign,
  knownCampaigns: IJsonCampaign[],
  offeredCamaign: IJsonCampaign,
  accMinutes: IAccCampaigns,
  hoursPastInWindow: number,
  currentlyWatchingInChrome: string): boolean {
  const offeredType = level(offeredCamaign)
  const offeredTypeRank = levelRank(offeredType)
  const offeredTypeInTopGroup = offeredType == "silver" || offeredType == "gold" || offeredType == "diamond"

  function shouldWatchBasedOnTime() {
    const stretchesSeen = accMinutes[offeredType].nonStopStretches
    const topGroupStretchesSeen = accMinutes.silver.nonStopStretches + accMinutes.gold.nonStopStretches + accMinutes.diamond.nonStopStretches
    if (stretchesSeen >= 2) {
      return false
    }

    if (offeredType == "silver") {
      if (CONFIG.silver == "smart") {
        console.log("Smart silver algo")
        if (hoursPastInWindow > 5 && (accMinutes.gold.nonStopStretches == 0 && accMinutes.diamond.nonStopStretches == 0)) {
          // no gold AND diamond ran yet, room for 2 high campaigns
          return true
        } else if (hoursPastInWindow > 11 && (accMinutes.gold.nonStopStretches == 0 || accMinutes.diamond.nonStopStretches == 0)) {
          // no gold OR diamond ran yet, room for 1 high campaigns
          return true
        } else if (hoursPastInWindow > 17 && (topGroupStretchesSeen <= 4)) {
          // room atleast a partial high campaigns
          return true
        } else {
          return false;
        }
      } else if (CONFIG.silver == "never") {
        console.log("Never silver algo")
        return false
      } else {
        console.log("Always silver algo")
        if(offeredTypeInTopGroup) {
          return stretchesSeen < 2 && topGroupStretchesSeen < 4
        } else {
          return stretchesSeen < 2 
        }        
      }
    } else {
      // still need atleast 1 stretch
      if(offeredTypeInTopGroup) {
        return stretchesSeen < 2 && topGroupStretchesSeen < 4
      } else {
        return stretchesSeen < 2 
      }        
    }
  }

  // if not watching now
  if (!watchingNow) {
    return shouldWatchBasedOnTime()
  } else {
    const currentType = watchingNow.type
    const currentTypeRank = levelRank(currentType)
    const minutesFollowed = (watchingNow.end - watchingNow.start) / 1000 / 60
    const minutesRemaining = (watchingNow.campaignEnd - moment.utc().valueOf()) / 1000 / 60

    if (currentTypeRank == offeredTypeRank) {
      console.log(`Test offeredCamaign.twitchUser [${offeredCamaign.twitchUser}] == currentlyWatchingInChrome [${currentlyWatchingInChrome}] : ${offeredCamaign.twitchUser == currentlyWatchingInChrome}`)
      if (offeredCamaign.twitchUser != currentlyWatchingInChrome) {
        if (minutesFollowed + minutesRemaining > 175) {
          //new user, but possibility to fill up minutes with current campaign
          return false
        } else {
          // be opportunistic, move to longer running campaign
          return offeredCamaign.minutesLeft > minutesRemaining
        }
      } else {
        return true;
      }
    } else if (currentTypeRank > offeredTypeRank && accMinutes[currentType].nonStopStretches < 2 && watchingNow.campaignEnd > moment.utc().valueOf() - 1000 * 60) {
      /// offered 1) lesser rank and 2) need current rank and 3) current not ended (end lies in future)
      console.log(`Currently watching a ${currentType} campaign that still needs watching, offered a ${offeredType}, no thanks`)
      return false
    } else {
      return shouldWatchBasedOnTime()
    }
  }
}

const idleUrl = chrome.extension.getURL("src/pages/drops.html?idle")
const configUrl = chrome.extension.getURL("src/pages/drops.html?config")


class CampaignWindow implements ICampaignWindow {
  time: number;
  campaigns: IInternalCampaign[];

  constructor(data: ICampaignWindow) {
    //{user, type, start, end}
    this.time = data.time
    this.campaigns = data.campaigns
    //{user, type, start, end}
  }

  getCurrentlyWatching(): IInternalCampaign | null {
    const lastWatched = this.campaigns[this.campaigns.length - 1]
    if (lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 < 5) {
      return lastWatched
    }
    else return null
  }

  watch(eitherCJsonOrUser: { cJson?: IJsonCampaign; internalCampaign?: IInternalCampaign }, tab: chrome.tabs.Tab): void {
    let cJson = eitherCJsonOrUser.cJson
    let internalCampaign = eitherCJsonOrUser.internalCampaign


    let now = moment.utc()

    if (cJson) {
      console.log(`Watching from cJson, doing clever stuff`)
      var url = `https://www.twitch.tv/${cJson.twitchUser}`
      if (tab.url != url) {
        chrome.tabs.update(tab.id, { url: url });
        this.campaigns.push({
          user: cJson.twitchUser,
          type: level(cJson),
          start: now.valueOf(),
          end: now.valueOf(),
          campaignEnd: cJson.endTime
        })
      } else {
        const lastWatched = this.campaigns[this.campaigns.length - 1]
        if (lastWatched && lastWatched.user == cJson.twitchUser && lastWatched.type == level(cJson)) {
          console.log("Update end of what is being watched")
          this.campaigns[this.campaigns.length - 1].end = now.valueOf()
        } else {
          this.campaigns.push({
            user: cJson.twitchUser,
            type: level(cJson),
            start: now.valueOf(),
            end: now.valueOf(),
            campaignEnd: cJson.endTime
          })
        }
      }
    }
    if (internalCampaign) {
      console.log(`Watching from internalCampaign, doing less clever stuff`)
      var url = `https://www.twitch.tv/${internalCampaign.user}`
      if (tab.url != url) {
        console.log("OOOPS, watch from lastcampaign, but tabs don't match, should still be on the tab, things broke")
        chrome.tabs.update(tab.id, {url: url})
      } else {
        const lastWatched = this.campaigns[this.campaigns.length - 1]
        if (lastWatched && lastWatched.user == internalCampaign.user && lastWatched.type == internalCampaign.type) {
          console.log("Update end of what is being watched")
          this.campaigns[this.campaigns.length - 1].end = now.valueOf()
        } else {
          console.log("OOOPS, watch from lastcamaign, but last campaign is different wtfbbq")
        }
      }
    }
    //console.log(JSON.stringify(this.campaigns))

    var camaignWindowKey = `campaignWindow-${this.time}`

    var newData = {}
    newData[camaignWindowKey] = {
      time: this.time,
      campaigns: this.campaigns
    }
    chrome.storage.local.set(newData, function () {
      console.log(`Update campaignWindow ${camaignWindowKey} with ${JSON.stringify(newData[camaignWindowKey])}`)
    });


  }

  wantToWatch(cJson: IJsonCampaign, knownCampaigns: IJsonCampaign[], currentlyWatchingInChrome: string): boolean {
    const capPerCampaign = 185;
    let now = moment.utc()
    let seen: IAccCampaigns = {
      bronze: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      silver: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      gold: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      diamond: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 }
    }
    this.campaigns.forEach(c => {
      const minutes = (c.end - c.start) / 1000 / 60
      seen[c.type].totalMinutes += minutes
      if (minutes > 178) {
        seen[c.type].nonStopStretches += 2
      } else if (minutes > 90) {
        seen[c.type].nonStopStretches += 1
      }

      seen[c.type].distinctUsers += 1
    });
    console.log(JSON.stringify(seen))

    // the highest level campaign always goes first, so no fear of accepting a low level one when there is a choice
    const currentlyWatching = this.getCurrentlyWatching()
    let hoursPastInWindow = now.diff(moment.utc(this.time), "hours")

    return shouldSwitch(currentlyWatching, knownCampaigns, cJson, seen, hoursPastInWindow, currentlyWatchingInChrome)
  }

  async update(): Promise<void> {
    let campaignCutoff = this.time

    // get currently following tabs
    let tabs: chrome.tabs.Tab[] = await new Promise(function (resolve, reject) {
      chrome.tabs.query({ "url": ["*://www.twitch.tv/*", idleUrl] }, function (tabs) {
        resolve(tabs)
      });
    })

    console.log(`Tabs managed by extension: ${tabs.length}`)

    // allow only one twitch tab
    if (tabs.length > 1) {
      for (let index = 1; index < tabs.length; index++) {
        chrome.tabs.remove(tabs[index].id)
      }
    }

    let currentlyWatchingInChrome = ""
    if (tabs.length != 0) {
      if (tabs[0].url != idleUrl) {
        checkHealth(tabs[0])
        let elements = `${tabs[0].url}`.split("/")
        currentlyWatchingInChrome = elements[elements.length - 1]
      }
    }

    let managedTab = tabs[0]
    if (!managedTab) {
      managedTab = await new Promise(function (resolve, reject) {
        chrome.tabs.create({ url: idleUrl }, function (tab) {
          resolve(tab)
        })
      })
    }

    const response = await fetch('https://eternalreplay.com/twitch/campaigns');
    const activeCampaigns = await response.json();

    // order active camaigns by most wanted frst
    activeCampaigns.sort((a, b) => {
      if (a.campaignType != b.campaignType) {
        //higher camaign level = better
        const types = ["bronze", "silver", "gold", "diamond"]
        const level_a = types.findIndex((t) => { return a.campaignType.toLowerCase().includes(t) })
        const level_b = types.findIndex((t) => { return b.campaignType.toLowerCase().includes(t) })
        return level_b - level_a
      } else {
        // the more time left, the better
        return b.minutesLeft - a.minutesLeft
      }
    })

    console.log(`Watching: ${currentlyWatchingInChrome} - Found camaigns on twitch: ${JSON.stringify(activeCampaigns)}`)

    let foundCamaignToWatch = false;

    for (let index = 0; index < activeCampaigns.length; index++) {
      const cJson = activeCampaigns[index];
      if (this.wantToWatch(cJson, activeCampaigns, currentlyWatchingInChrome)) {
        console.log("Want to see: " + JSON.stringify(cJson))
        foundCamaignToWatch = true;
        this.watch({ cJson: cJson }, managedTab)
        break;
      }
    }

    if (!foundCamaignToWatch) {
      const lastWatched = this.campaigns[this.campaigns.length - 1]
      if (lastWatched && lastWatched.campaignEnd > moment.utc().valueOf()) {
        // still watching some camign, keep it open just to be sure as there is nothing better       
        console.log("Nothing needed to watch, but current campaign is still running, let's keep following")
        this.watch({ internalCampaign: lastWatched }, managedTab)
      }
      if (!lastWatched || lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 > 5) {
        console.log("Nothing watched for 5 min, cleaning up")
        if (managedTab && managedTab.url != idleUrl) {
          chrome.tabs.update(managedTab.id, { url: idleUrl });
        }
      }
    }
  }
}


async function getCamaignWindow(): Promise<CampaignWindow> {
  let campaignCutoff = moment.utc("08", "hh")
  let now = moment.utc()

  if (now.isBefore(campaignCutoff)) {
    campaignCutoff.subtract(1, "d")
  }

  var camaignWindowKey = `campaignWindow-${campaignCutoff.valueOf()}`

  let p: Promise<CampaignWindow> = new Promise(function (resolve, reject) {
    chrome.storage.local.get([camaignWindowKey], function (result) {
      if (!result[camaignWindowKey]) {
        var newData = {}
        newData[camaignWindowKey] = {
          time: campaignCutoff.valueOf(),
          campaigns: []
        }
        chrome.storage.local.set(newData, function () {
          console.log(`Initializing campaignWindow ${camaignWindowKey} with ${JSON.stringify(newData[camaignWindowKey])}`)
          resolve(new CampaignWindow(newData[camaignWindowKey]))
        });
      } else {
        resolve(new CampaignWindow(result[camaignWindowKey]))
      }
    });
  });

  return await p
}

chrome.browserAction.onClicked.addListener(function (tab) {
  chrome.tabs.create({ url: configUrl })
});



chrome.alarms.create("madness", { when: Date.now(), periodInMinutes: 1 })
chrome.alarms.onAlarm.addListener(async (a) => {
  let config = await loadConfig();
  console.log(`Running dropocalypse with config: ${JSON.stringify(CONFIG)}`)
  if(CONFIG.enabled) {    
    let window = await getCamaignWindow();
    window.update()
  }
})


function patchCSP(requestDetails) {
  var newHeaders = []
  requestDetails.responseHeaders.forEach(element => {
    if (element.name == "content-security-policy") {
      element.value = element.value.replace("script-src 'self'", "script-src 'self' 'unsafe-inline'")
      newHeaders.push(element)
    } else {
      newHeaders.push(element)
    }
  });
  return { responseHeaders: newHeaders };
}

chrome.webRequest.onHeadersReceived.addListener(
  patchCSP,
  {
    urls: [
      "https://*.ext-twitch.tv/*viewer*"
    ]
  },
  ["blocking", "responseHeaders"]
);



/* 
Stats are fun. let's store them
*/
chrome.storage.local.get(['dropCountStart'], function (result) {
  if (!result['dropCountStart']) {
    chrome.storage.local.set({ 'dropCountStart': moment().unix() }, function () {
      console.log("Initializing counter")
    });
  } else {
    console.log("Counter started at " + result['dropCountStart'])
  }
});

function countDrop(accepted, amount) {
  var m = moment()
  var groupKey = `dropDay.${m.year()}.${m.dayOfYear()}`
  var data = {
    'unix': m.unix(),
    'accepted': accepted,
    'amount': amount
  }
  chrome.storage.local.get([groupKey], function (result) {
    if (!result[groupKey]) {
      var newData = {}
      newData[groupKey] = [data]
      chrome.storage.local.set(newData, function () {
        console.log(`Initializing daily accumulator for ${groupKey} with ${JSON.stringify([data])}`)
      });
    } else {
      result[groupKey].push(data)
      chrome.storage.local.set(result, function () {
        console.log(`Pusshed data to daily accumulator for ${groupKey}. It now holds ${JSON.stringify(result[groupKey])}`)
      });
    }
  });
}

/*
Our listener
 */
let canAcceptDrops = true;
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  //console.log(sender.tab ? "MSG from a content script:" + sender.tab.url : "MSG from the extension");  
  if (request.t == "GotDrop") {
    console.log(`Got drop: ${sender.tab.url} ${JSON.stringify(request.dropEvent)} - ${CONFIG.chests ? "Trying to claim chest" : "Ignoring chests"}`)
    if (canAcceptDrops && CONFIG.chests) {
      const randomFactor = Math.floor(Math.random() * 100); 
      if(randomFactor > 20) {
        countDrop(true, request.dropEvent.currencyGranted)
        var opt = {
          type: "basic",
          title: "Drop!",
          message: `You recieved ${request.dropEvent.currencyGranted} blue thingies!`,
          iconUrl: "/icons/icon128.png",
          silent: true
        }
        chrome.notifications.create(opt, function (id) {
          setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
        })
        //
        var takeDrop = {
          "nonce": request.dropEvent.nonce,
          "twitchId": request.dropEvent.twitchId,
          "accountId": request.dropEvent.accountId,
          "messageType": "DropAccepted"
        }
        sendResponse(takeDrop)        
      } else {
        var opt = {
          type: "basic",
          title: "Missed Drop!",
          message: `You were offered ${request.dropEvent.currencyGranted} blue thingies. Unfortunately you were afk.`,
          iconUrl: "/icons/icon128.png",
          silent: true
        }
        chrome.notifications.create(opt, function (id) {
          setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
        })
      }
      canAcceptDrops = false
      setTimeout(function () { canAcceptDrops = true }, 1000 * 60)      
    } else {
      console.log(`Drop from ${sender.tab.url} is not processed`)
    }
  } else if (request.t == "MiscWsFrame") {
    //console.log(`Misc frame from: ${sender.tab.url}`)
    updateTwitchSeen(sender.tab.url)    
    sendResponse()
  }
});