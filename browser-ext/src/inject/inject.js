/**
 * Wrap websocket so we can interact with it
 */
var actualCode = `
		function log(d) {
			var evt=document.createEvent("CustomEvent");
			evt.initCustomEvent("EternalDropsContentEvent-Log", true, true, d);
			document.dispatchEvent(evt);
		}

		function incoming(d, key) {
			var evt=document.createEvent("CustomEvent");
			evt.initCustomEvent("EternalDropsContentEvent-Incoming", true, true, {data: d, key: key});
			document.dispatchEvent(evt);
		}

		activeWS = new Map();
		activeWSKey = 0;		

		document.addEventListener('EternalDropsContentEvent-Send', function (e) {
			var d = e.detail;			
			var data = d.data;
			var key = d.key;
			log("sending to ws id "+ key +" data "+ data)
			activeWS.get(key).send(data)
		})

		 WebSocket.prototype._send = WebSocket.prototype.send;
		 WebSocket.prototype.send = function (data) {
		 	this.wsKey = ++activeWSKey
		 	activeWS.set(this.wsKey, this)
		 	this._send(data);
		 	this.addEventListener('message', function (msg) {
		 		incoming(msg.data, this.wsKey)
		 		//log('<< ' + msg.data);
		 	}, false);
		 	this.send = function (data) {
		 		this._send(data);
		 		//log(">> " + data);
		 	};
		 	//log(">> " + data);
		 } 
`;

chrome.extension.sendMessage({}, function (response) {
	document.onreadystatechange = function () {
		if (document.readyState === "interactive") {
			//clearInterval(readyStateCheckInterval);

			var dropsRoot = document.querySelector(".drops-notification")

			document.querySelectorAll("script").forEach(function(s) {
				console.log(s.getAttribute("src"))
			})

			console.log("Hello. This message was sent from scripts/inject.js [" + (null == dropsRoot ? "no-drops" : "drops") + "]");
			console.log(document.location);

			function sendToWS(d, key) {
				var evt=document.createEvent("CustomEvent");
				evt.initCustomEvent("EternalDropsContentEvent-Send", true, true, {data: d, key: key});
				document.dispatchEvent(evt);
			}

			if (null != dropsRoot) {
				console.log("We got drops, injecting script")
				document.addEventListener('EternalDropsContentEvent-Incoming', function (e) {
					var d = e.detail;					
					var data = JSON.parse(d.data)
					if (data.messageType == "DropNotification") {	
							console.log(d);				
							chrome.runtime.sendMessage({t: "GotDrop", dropEvent: data}, (response) => {
								if(response) {
									console.log(response)
									setTimeout(() => {sendToWS(JSON.stringify(response), d.key)}, Math.floor(Math.random() * 7500) + 3432)									
								}								
							 });						 
					} else {
						chrome.runtime.sendMessage({t: "MiscWsFrame", data: data, url: document.location}, (response) => {
							if(response) {
								console.log(response)
								 						
							}								
						 });		
					}
				}
				);

				document.addEventListener('EternalDropsContentEvent-Log', function (e) {
					var data = e.detail
					console.log(data);
				});

				var script = document.createElement('script');
				script.textContent = actualCode;
				(document.head||document.documentElement).appendChild(script);
				script.remove();
				//document.documentElement.setAttribute('onreset', actualCode);
				//document.documentElement.dispatchEvent(new CustomEvent('reset'));
				//document.documentElement.removeAttribute('onreset');
			}




		}
	};
});