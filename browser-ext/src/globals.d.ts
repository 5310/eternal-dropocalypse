interface IJsonCampaign {
    campaignType: string;
    endTime: number;
    endUTC: string;
    minutesLeft: number;
    twitchUser: string;
  }
  
  interface IInternalCampaign {
    user: string;
    type: string;
    start: number;
    end: number;
    campaignEnd: number;
  }

  interface IAccData {
    totalMinutes: number;
    nonStopStretches: number;
    distinctUsers: number;
  }
  
  interface IAccCampaigns {
    bronze: IAccData;
    silver: IAccData;
    gold: IAccData;
    diamond: IAccData;
  }
  
  interface ICampaignWindow {
    time: number;
    campaigns: IInternalCampaign[];
  }
  
  declare enum SilverChoice {
    NEVER = "never",
    ALWAYS = "always",
    SMART = "smart",
  }