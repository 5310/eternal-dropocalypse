/*import ko from "knockout"
import ksb from "./ko.secure"


 
var options = {
   attribute: "data-bind",        // default "data-sbind"
   globals: window,               // default {}
   bindings: ko.bindingHandlers,  // default ko.bindingHandlers
   noVirtualElements: false       // default true
};
ko.bindingProvider.instance = ksb(options);
*/

import tinybind from 'tinybind'
import moment from 'moment'; 
chrome.storage.local.get(null, function (result) {
  let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("campaignWindow") })
  let campaignWindows = []
  let ct = 0;
  keys.reverse().forEach((k) => {    
    let cw = result[k]
    let friendlyTime = moment.utc(cw.time)

    if(ct++ <= 7) {
      campaignWindows.push({
        cw: friendlyTime.local().format("dddd, MMMM Do YYYY"),
        watched: cw.campaigns.map((c) => {
          return {
            streamer: c.user,
            type: c.type,
            minutes: Math.round(0 + (c.end - c.start) / 1000 / 60),
            start: moment.utc(c.start).local().format("HH:mm")
          }
        })
      })
    }
  })

  tinybind.bind(document.getElementById("campaigns"), {
    campaignWindows: campaignWindows
  })
});


import WatchJS from 'melanke-watchjs';
const watch = WatchJS.watch;
  
chrome.storage.local.get(["extension-config"], function(result) {
  let config = {
    chests: "no",
    enabled:"yes",
    silver: "smart"
  }
  if(result["extension-config"]) {
    config = result["extension-config"]
  }

  tinybind.bind(document.getElementById("config"), {config: config})

  watch(config, function(){
    console.log(`Changes, beautiful changes ${JSON.stringify(config)}`);
    chrome.storage.local.set({"extension-config": config})
  });

  chrome.storage.onChanged.addListener(function (changes, area) {
    if (area == "local" && changes["extension-config"]) {
      const newValue = changes["extension-config"].newValue
      console.log(`Onchange fired, beautiful changes ${JSON.stringify(newValue)}`);
      config.chests = newValue.chests
      config.enabled = newValue.enabled
      config.silver = newValue.silver
    }
  })
})

