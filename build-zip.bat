@echo off

call parcel build browser-ext\manifest.json browser-ext\src\pages\*.html  && cat dist\manifest.json | sed s;\\;/;g > dist\manifest.json.uf && mv dist\manifest.json.uf dist\manifest.json

rmdir /S /Q dist.chrome
mkdir dist.chrome
xcopy dist dist.chrome /s /e /y

call json -f dist.chrome/manifest.json -e "delete this.key" > swap.txt
mv swap.txt dist.chrome/manifest.json

rm dist.chrome.zip
jar -cMf dist.chrome.zip dist.chrome